<h1>ConfigMaker</h1>

<h2>Using:</h2>

<h3>File: config/data.txt:</h3>

<pre>app.data.account.id=127              
app.data.account.money=12575         
app.data.account.user.name=alex1rap  
app.data.settings.synchronizing=true 
app.data.account.user.age=20</pre>
<hr>

<h3>PHP Script:</h3>

...
<pre>$config = alex1rap\helpers\ConfigMaker::fromFile(__DIR__ . "/config/data.txt");

print_r($config);</pre>
...
<hr>

<h3>Result is:</h3>

<pre>Array
(
    [app] => Array
        (
            [data] => Array
                (
                    [account] => Array
                        (
                            [id] => 127
                            [money] => 12575
                            [user] => Array
                                (
                                    [name] => alex1rap
                                    [age] => 20
                                )
                        )
                    [settings] => Array
                        (
                            [synchronizing] => true
                        )
                )
        )
)</pre>