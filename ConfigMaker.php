<?php

namespace alex1rap\helpers;

class ConfigMaker
{

    /**
     * @param string $fileName
     * @return array
     */
    public static function fromFile(string $fileName): array
    {
        $data = file($fileName);
        $result = [];
        foreach ($data as $string) {
            $string = trim($string);
            
            if (empty($string)) continue;

            $params = explode("=", $string, 2);
            $value = $params[1];
            $keys = explode(".", $params[0]);
            $result = array_merge_recursive($result
                , self::makeArray($keys, $value));
        }
        return $result;
    }

    /**
     * @param array $keys
     * @param $value
     * @param int $depth
     * @return mixed
     */
    protected static function makeArray(array $keys, $value, int $depth = 0)
    {
        if ($depth < count($keys)) {
            $result = [];
            $result[$keys[$depth]] = self::makeArray($keys, $value, ++$depth);
        } else {
            $result = $value;
        }
        return $result;
    }
}
